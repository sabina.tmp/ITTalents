var isPause = false;

function  downloadData() {
    var url = "http://localhost:11144/service/api.php";

    // temporarly use relative url
    var url = "/service/api.php";

    $.ajax({url: url, success: function(result){
        var json = result;

        var i = 0;
        while(i < json.length)
        {
            var long = json[i].longitude;
            var lat = json[i].latitude;

            if(isPause == false)
                getAddressAndCreateMarker(lat, long);

            i++;
            return;
        }
    }});
}

var map = new google.maps.Map(document.getElementById('someMeaningfulId'), {
    center: {lat: 52, lng: 10},
    scrollwheel: false,
    zoom: 5
});


var geocoder;

function initialize() {
    geocoder = new google.maps.Geocoder();
}

function getAddressAndCreateMarker(lat, long) {
    var latlng = new google.maps.LatLng(lat, long);

    geocoder.geocode({
        'latLng': latlng
    }, function (results, status) {

        var address;
        if (status === google.maps.GeocoderStatus.OK) {
            if (results[1]) {
                address = results[1].formatted_address;
            } else {
                address = 'Address not found';
            }
        } else {
            address = 'Geocoder failed due to: ' + status;
        }

        createMarker(lat, long, address);
    });
}

google.maps.event.addDomListener(window, 'load', initialize);

var timer;
function  startTimer() {
    timer = setTimeout(function(){ downloadData(); startTimer(); }, 1000);
}

var image = {
    url: '/favicon.png',
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 32)
};

var image_dollar = {
    url: '/dollar_1.png',
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 32)
};

var image_dollar2 = {
    url: '/dollar_2.png',
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 32)
};

var markers = [];

function createMarker(lat, long, address) {

    var myLatLng = {"lat": parseFloat(lat), "lng": parseFloat(long)};
    //console.log(latlng);

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: null,
        icon: (Math.random() < 0.5 ? image_dollar : image_dollar2)
    });

    // add mouseover info window for each marker
    var infowindow = new google.maps.InfoWindow({
        content: address
    });

    marker.addListener('mouseover', function() {
        infowindow.open(map, marker);
    });

    markers.push(marker);

    setTimeout(function(){

        if(isPause == false)
            marker.setMap(map);

        marker.setAnimation(google.maps.Animation.BOUNCE);

        setTimeout(function()
        {
            if(isPause == false)
                marker.setMap(null);

        }, Math.random() * 300 * sliderValue);
    }, Math.random() * 2000);
}

var sliderValue = 1;

$(document).ready(function () {
    $('#ex1').slider({
        formatter: function(value) {
            return 'Current value: ' + value;
        }
    }).on('slide', function (val) {
        sliderValue = val.value;
    });

    startTimer();

    $pauseButton = $('#pauseBtn');
});

var $pauseButton;
function togglePause() {
    isPause = !isPause;

    if (isPause && timer) {
        clearTimeout(timer);
        $pauseButton.val('Start');
    } else {
        startTimer();
        $pauseButton.val('Pause');
    }
}