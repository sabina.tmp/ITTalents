<?php

class Transactions
{
    /**
     * @var database connection
     */
    private $db;

    /**
     * Transactions constructor.
     * @param $db
     */
    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * @param $count
     * @return array
     * @throws Exception
     */
    public function get($count)
    {
        $sql = 'SELECT * FROM transactions ORDER BY RAND() LIMIT ' . $count;
        $result = $this->query($sql);

        $data = [];
        while ($transaction = $result->fetch_assoc()) {
            $data[] = $transaction;
        }

        return $data;
    }
    
    public function delete($id) {}
    public function create() {}
    public function update() {}

    /**
     * @param $sql
     * @return mixed
     * @throws Exception
     */
    protected function query($sql)
    {
        if ($sql && !$result = $this->db->query($sql)) {
            throw new Exception('Query failed: ' . $this->db->error);
        }

        return $result;
    }
}