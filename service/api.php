<?php

/*
Task 2:

If I had more time, I would:
... use a lightweight framework for the API (Sylex, Lumen, Slim etc) This would allow me to easily format the urls so that they would be cleaner
... use a separate class for the database connection, making the project easier to maintain and extend.
... create all the functionality of the REST API: no only the listing, but also creating, updating and deleting.
... probably add a setting to output the data either in JSON or XML
... better document the API
... write tests to test the API

*/

$conn = new mysqli('127.0.0.1', 'root', '1', 'ittalent');

if ($conn->connect_errno) {
    echo "Sorry, this website is experiencing problems.";
    exit;
}

require_once 'Transactions.php';
$transactions = new Transactions($conn);

$method = $_SERVER['REQUEST_METHOD'];
$data = [];

// create SQL based on HTTP method
switch ($method) {
    case 'GET':
        try {
            $data = $transactions->get(20);
        } catch (Exception $e) {
            echo 'An error has occured';
            die();
        }
        break;
    case 'PUT':
        break;
    case 'POST':
        break;
    case 'DELETE':
        break;
}

// get only the elements we need
foreach ($data as $key => $transaction) {
    $data[$key] = [
        'latitude' => $transaction['latitude'],
        'longitude' => $transaction['longitude'],
    ];
}

header('Content-Type: application/json');
echo json_encode($data);